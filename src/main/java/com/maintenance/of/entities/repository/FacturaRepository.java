package com.maintenance.of.entities.repository;

import com.maintenance.of.entities.domain.Category;
import com.maintenance.of.entities.domain.Factura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FacturaRepository extends JpaRepository<Factura, Long> {
//mal
    /**
     @Query(value = "SELECT invoice_number FROM factura c  WHERE c.invoice_number=:invoice_number")
     String findInvoiceNumber(@Param("invoice_number") String invoiceNumber) throws Exception;
     **/
    Factura findByInvoiceNumber(String invoice_number);


}
