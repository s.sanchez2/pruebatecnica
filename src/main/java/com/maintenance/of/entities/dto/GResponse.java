package com.maintenance.of.entities.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GResponse {
    private String codeStatus;
    private String message;
}
