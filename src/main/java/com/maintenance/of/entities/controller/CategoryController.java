package com.maintenance.of.entities.controller;
import com.maintenance.of.entities.dto.request.CategoryDtoRequest;
import com.maintenance.of.entities.service.CategoryService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/categories")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @ApiOperation("Listar una categria por ID")
    @GetMapping("/{id}")
    public Object findCategoryById(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.findCategoryById(id));
    }

    @ApiOperation("Guardar una categoria")
    @PostMapping
    public Object saveCategory(@RequestBody CategoryDtoRequest categoryDtoRequest) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.saveCategory(categoryDtoRequest));
    }

    @ApiOperation("Actualizar una categoria")
    @PutMapping("/{id}")
    public Object updateCategory(@PathVariable("id") Long id, @RequestBody CategoryDtoRequest categoryDtoRequest) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.updateCategory(id,categoryDtoRequest));
    }

    @ApiOperation("Eliminar una categoria")
    @DeleteMapping("/{id}")
    public Object deletedCategory(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.deletedCategory(id));
    }

    @ApiOperation("Eliminacion logica de una categoria")
    @DeleteMapping("logic/{id}")
    public Object deletedLogicCategory(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.deletedLogicCategory(id));
    }

}
