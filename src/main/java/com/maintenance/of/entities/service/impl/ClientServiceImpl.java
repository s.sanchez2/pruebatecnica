package com.maintenance.of.entities.service.impl;

import com.maintenance.of.entities.domain.Client;
import com.maintenance.of.entities.domain.Product;
import com.maintenance.of.entities.dto.request.ClientDtoRequest;
import com.maintenance.of.entities.dto.response.ClientDtoResponse;
import com.maintenance.of.entities.repository.CategoryRepository;
import com.maintenance.of.entities.repository.ClientRepository;
import com.maintenance.of.entities.repository.ProductRepository;
import com.maintenance.of.entities.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Value("${zoneId}")
    private String zoneId;

    @Override
    public Object findClient(Long id) throws Exception {
        Optional<Client> clientOptional = Optional.ofNullable(clientRepository.findByIdAndState(id, true));

        if (clientOptional.isPresent()) {

            Client client = clientOptional.get();

            return ClientDtoResponse.builder()
                    .id(client.getId())
                    .name(client.getName())
                    .address(client.getAddress())
                    .phone(client.getPhone())
                    .state(client.getState())
                    .createdAt(client.getCreatedAt())
                    .modifiedAt(client.getModifiedAt())
                    .build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND);
    }

    @Override
    public Object saveClient(ClientDtoRequest clientsave) throws Exception {

        Client client = Client.builder()
                .name(clientsave.getName())
                .address(clientsave.getAddress())
                .phone(clientsave.getPhone())
                .state(true)
                .createdAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()))
                .modifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()))
                .build();

        return clientRepository.save(client);

    }

    @Override
    public Object updateClient(Long id, ClientDtoRequest clientsave) throws Exception {
        if (clientRepository.existsById(id)) {
            Client client = clientRepository.findByIdAndState(id, true);

            client.setName(clientsave.getName());
            client.setAddress(clientsave.getAddress());
            client.setPhone(client.getPhone());
            client.setModifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()));

            return clientRepository.save(client);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND);
}

    @Override
    public Object deletedClient(Long id) throws Exception {
        if (!clientRepository.existsById(id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        clientRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public Object deletedLogicClient(Long id) throws Exception {
        if (!clientRepository.existsById(id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        Client client = clientRepository.findByIdAndState(id, true);
        client.setState(false);
        client.setModifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()));
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
