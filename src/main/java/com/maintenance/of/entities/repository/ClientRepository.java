package com.maintenance.of.entities.repository;

import com.maintenance.of.entities.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByIdAndState(Long id, Boolean state);

    @Query(value = "SELECT COUNT(c.id) FROM Client c  WHERE c.name=:name")
    Integer findName(@Param("name") String name) throws Exception;

}
