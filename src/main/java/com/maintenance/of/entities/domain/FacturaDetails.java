package com.maintenance.of.entities.domain;

import lombok.*;
import javax.persistence.*;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder

@Entity
@Table(name = "factura_details")
public class FacturaDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String paymentType;
    private int numberItems;
    private double TotalCost;

    @JoinColumn(name = "factura_id", referencedColumnName = "id")
    @OneToOne
    private Factura factura;

    @JoinColumn(name = "product_id", referencedColumnName = "id")
    @ManyToOne
    private Product produc;

    private Date createdAt;
    private Boolean state;
}
