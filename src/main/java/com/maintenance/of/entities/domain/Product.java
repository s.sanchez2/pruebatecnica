package com.maintenance.of.entities.domain;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder

@Entity
@Table(name = "product")
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Double price;

    @JoinColumn(name = "category_id", referencedColumnName = "id")
    @OneToOne
    private Category category;

    private Boolean state;
    private Date createdAt;
    private Date modifiedAt;
}
