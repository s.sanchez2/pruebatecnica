package com.maintenance.of.entities.service;

import com.maintenance.of.entities.dto.request.ClientDtoRequest;


public interface ClientService {
    Object findClient(Long id) throws Exception;
    Object saveClient(ClientDtoRequest clientsave) throws Exception;
    Object updateClient(Long id, ClientDtoRequest clientsave) throws Exception;
    Object deletedClient(Long id) throws Exception;
    Object deletedLogicClient(Long id) throws Exception;
}
