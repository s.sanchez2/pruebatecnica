package com.maintenance.of.entities.controller;

import com.maintenance.of.entities.dto.request.FacturaDtoRequest;
import com.maintenance.of.entities.dto.request.ProductDtoRequest;
import com.maintenance.of.entities.service.FacturaService;
import com.maintenance.of.entities.service.ProductService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/factura")
@RequiredArgsConstructor
public class FacturaController {
    private final FacturaService facturaService;

    @ApiOperation("Listar una por numero de fcatura")
    @GetMapping("/{Invoice_number}")
    public Object findFacturaByInvoiceNumber(@RequestParam("Invoice_number") String InvoiceNumber) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(facturaService.findFacturaByInvoiceNumber(InvoiceNumber));
    }

    @ApiOperation("Guardar una factura")
    @PostMapping
    public Object saveProduct(@RequestBody FacturaDtoRequest facturasave) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(facturaService.saveFactura(facturasave));
    }
}
