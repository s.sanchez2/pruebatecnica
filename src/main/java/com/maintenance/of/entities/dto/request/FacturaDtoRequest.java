package com.maintenance.of.entities.dto.request;

import com.maintenance.of.entities.domain.Client;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class FacturaDtoRequest {
    private String invoiceNumber;
    private ClientDtoRequestFactura client;

}
