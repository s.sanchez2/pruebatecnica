package com.maintenance.of.entities.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.FileReader;

public class Generic {
    public static JsonArray readJson(String fileName) {
        JsonArray json = null;
        try {
            JsonParser parser = new JsonParser();
            Object obj = parser.parse(new FileReader(fileName));
            json = (JsonArray) obj;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return json;
    }
}
