package com.maintenance.of.entities.controller;

import com.maintenance.of.entities.dto.request.CategoryDtoRequest;
import com.maintenance.of.entities.dto.request.ProductDtoRequest;
import com.maintenance.of.entities.service.CategoryService;
import com.maintenance.of.entities.service.ProductService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @ApiOperation("Listar un producto por ID")
    @GetMapping("/{id}")
    public Object findProductById(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(productService.findProductById(id));
    }

    @ApiOperation("Guardar una producto")
    @PostMapping
    public Object saveProduct(@RequestBody ProductDtoRequest productsave) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(productService.saveProduct(productsave));
    }

    @ApiOperation("Actualizar una producto")
    @PutMapping("/{id}")
    public Object updateProduct(@PathVariable("id") Long id, @RequestBody ProductDtoRequest productsave) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(productService.updateProduct(id,productsave));
    }

    @ApiOperation("Eliminar un Producto")
    @DeleteMapping("/{id}")
    public Object deletedCategory(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(productService.deletedProduct(id));
    }

    @ApiOperation("Eliminacion logica de un Producto")
    @DeleteMapping("logic/{id}")
    public Object deletedLogicCategory(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(productService.deletedLogicProduct(id));
    }
}
