package com.maintenance.of.entities.service;
import com.maintenance.of.entities.dto.request.CategoryDtoRequest;

public interface CategoryService {

    Object findCategoryById(Long id) throws Exception;
    Object saveCategory(CategoryDtoRequest categorySave) throws Exception;
    Object updateCategory(Long id,CategoryDtoRequest categoryUpdate) throws Exception;
    Object deletedCategory(Long id) throws Exception;
    Object deletedLogicCategory(Long id) throws Exception;

}
