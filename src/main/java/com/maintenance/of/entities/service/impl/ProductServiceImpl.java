package com.maintenance.of.entities.service.impl;

import com.maintenance.of.entities.domain.Category;
import com.maintenance.of.entities.domain.Product;
import com.maintenance.of.entities.dto.request.CategoryDtoRequest;
import com.maintenance.of.entities.dto.request.ProductDtoRequest;
import com.maintenance.of.entities.dto.response.CategoryDtoResponse;
import com.maintenance.of.entities.dto.response.ProductDtoResponse;
import com.maintenance.of.entities.repository.CategoryRepository;
import com.maintenance.of.entities.repository.ProductRepository;
import com.maintenance.of.entities.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    @Value("${zoneId}")
    private String zoneId;

    @Override
    public Object findProductById(Long id) throws Exception {
        Optional<Product> productOptional = Optional.ofNullable(productRepository.findByIdAndState(id, true));

        if (productOptional.isPresent()){

            Product product = productOptional.get();
            CategoryDtoRequest categoryDtoRequest = CategoryDtoRequest.builder()
                    .name(product.getCategory().getName())
                    .build();

            return ProductDtoResponse.builder()
                    .id(product.getId())
                    .name(product.getName())
                    .price(product.getPrice())
                    .state(product.getState())
                    .category(categoryDtoRequest)
                    .createdAt(product.getCreatedAt())
                    .modifiedAt(product.getModifiedAt())
                    .build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND);
    }

    @Override
    public Object saveProduct(ProductDtoRequest productsave) throws Exception {
        Integer findName = productRepository.findName(productsave.getName());

        if (findName == 0 ){
            Optional<Category> categoryOptional = categoryRepository.findById(productsave.getCategory().getId());
            if (categoryOptional.isPresent()){
                Category category = categoryOptional.get();

                Product product = Product.builder()
                        .name(productsave.getName())
                        .price(productsave.getPrice())
                        .state(true)
                        .category(category)
                        .createdAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()))
                        .modifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()))
                        .build();
                return productRepository.save(product);
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(HttpStatus.IM_USED);

    }

    @Override
    public Object updateProduct(Long id, ProductDtoRequest productsave) throws Exception {

        if (productRepository.existsById(id)){
            Product product = productRepository.findByIdAndState(id, true);
            Optional<Category> categoryOptional = categoryRepository.findById(productsave.getCategory().getId());

            if (categoryOptional.isPresent()){
                Category category = categoryOptional.get();

                product.setName(productsave.getName());
                product.setPrice(productsave.getPrice());
                product.setCategory(category);
                product.setModifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()));

                return productRepository.save(product);
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND);
    }

    @Override
    public Object deletedProduct(Long id) throws Exception {
        if (!productRepository.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        productRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public Object deletedLogicProduct(Long id) throws Exception {
        if (!productRepository.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        Product product = productRepository.findByIdAndState(id, true);
        product.setState(false);
        product.setModifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()));

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
