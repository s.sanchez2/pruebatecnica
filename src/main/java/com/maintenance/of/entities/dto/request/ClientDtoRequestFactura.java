package com.maintenance.of.entities.dto.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ClientDtoRequestFactura {
    private Long id;

}
