package com.maintenance.of.entities.service;

import com.maintenance.of.entities.dto.request.ProductDtoRequest;

public interface ProductService {

    Object findProductById(Long id) throws Exception;
    Object saveProduct(ProductDtoRequest productsave) throws Exception;
    Object updateProduct(Long id,ProductDtoRequest productsave) throws Exception;
    Object deletedProduct(Long id) throws Exception;
    Object deletedLogicProduct(Long id) throws Exception;

}
