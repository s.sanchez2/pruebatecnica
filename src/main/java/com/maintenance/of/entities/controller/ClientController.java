package com.maintenance.of.entities.controller;

import com.maintenance.of.entities.dto.request.ClientDtoRequest;
import com.maintenance.of.entities.service.ClientService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/client")
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @ApiOperation("Listar un usuario por ID")
    @GetMapping("/{id}")
    public Object findClientById(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findClient(id));
    }

    @ApiOperation("Guardar un nuevo usuario")
    @PostMapping
    public Object saveClient(@RequestBody ClientDtoRequest clientsave) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.saveClient(clientsave));
    }

    @ApiOperation("Actualizar un usuario")
    @PutMapping("/{id}")
    public Object updateClient(@PathVariable("id") Long id, @RequestBody ClientDtoRequest clientsave) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.updateClient(id,clientsave));
    }

    @ApiOperation("Eliminar un usuario")
    @DeleteMapping("/{id}")
    public Object deletedClient(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.deletedClient(id));
    }

    @ApiOperation("Eliminacion logica de un usuario")
    @DeleteMapping("logic/{id}")
    public Object deletedLogicClient(@PathVariable("id") Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.deletedLogicClient(id));
    }
}
