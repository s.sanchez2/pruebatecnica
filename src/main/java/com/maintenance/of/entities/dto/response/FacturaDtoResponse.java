package com.maintenance.of.entities.dto.response;

import com.maintenance.of.entities.dto.request.ClientDtoRequest;
import com.maintenance.of.entities.dto.request.ClientDtoRequestFactura;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class FacturaDtoResponse {

    private Long id;
    private String invoiceNumber;
    private ClientDtoRequestFactura client;
    private Date createdAt;

}
