package com.maintenance.of.entities.service.impl;

import com.maintenance.of.entities.domain.Category;
import com.maintenance.of.entities.dto.request.CategoryDtoRequest;
import com.maintenance.of.entities.dto.response.CategoryDtoResponse;
import com.maintenance.of.entities.repository.CategoryRepository;
import com.maintenance.of.entities.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class CategoryServiceImpl implements CategoryService {
    /**
     * CategoryRepository, injeccion de dependencia, @RequiredArgsConstructor inyecta en el constructor interno
     * **/
    private final CategoryRepository categoryRepository;

    /**
     * zoneId, zona horaria establecida en properties
     * **/
    @Value("${zoneId}")
    private String zoneId;

    @Override
    public Object findCategoryById(Long id) throws Exception {
        /**
         * findByIdAndState, devuelve un objeto solo con estado activo o true
         * **/
        Optional<Category> categoryOptional = Optional.ofNullable(categoryRepository.findByIdAndState(id, true));

        if (categoryOptional.isPresent()){
            Category category = categoryOptional.get();
            return CategoryDtoResponse.builder()
                    .id(category.getId())
                    .name(category.getName())
                    .state(category.getState())
                    .createdAt(category.getCreatedAt())
                    .modifiedAt(category.getModifiedAt())
                    .build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND);
    }

    @Override
    public Object saveCategory(CategoryDtoRequest categorySave) throws Exception {
        /**
         * findName, retorna la cantidad de veces que se repite el nombre, si es 0 el nombre está disponible
         * **/
        Integer findName = categoryRepository.findName(categorySave.getName());
        if (findName == 0 ){

          Category  category = Category.builder()
                    .name(categorySave.getName())
                    .state(true)
                    .createdAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()))
                    .modifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()))
                    .build();

            return categoryRepository.save(category);
        }
        return ResponseEntity.status(HttpStatus.IM_USED);

    }

    @Override
    public Object updateCategory(Long id, CategoryDtoRequest categoryUpdate) throws Exception {

        Optional<Category> categoryOptional = Optional.ofNullable(categoryRepository.findByIdAndState(id, true));

        if (categoryRepository.existsById(id)){
            if (categoryOptional.isPresent()){
                Category category = categoryOptional.get();

                category.setName(categoryUpdate.getName());
                category.setModifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()));
                return categoryRepository.save(category);
            }else
                return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(HttpStatus.IM_USED);

    }

    @Override
    public Object deletedCategory(Long id) throws Exception {
        if (!categoryRepository.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        categoryRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public Object deletedLogicCategory(Long id) throws Exception {
        if (!categoryRepository.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        Category category = categoryRepository.findByIdAndState(id, true);
        category.setState(false);
        category.setModifiedAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()));

        return new ResponseEntity<>(HttpStatus.OK);
    }


}
