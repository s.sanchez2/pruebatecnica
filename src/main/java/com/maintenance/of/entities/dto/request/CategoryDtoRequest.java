package com.maintenance.of.entities.dto.request;

import lombok.*;

import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class CategoryDtoRequest {

    @Size(min = 1, max = 20, message = "CATEGORY_NAME_SIZE")
    private String name;
}
