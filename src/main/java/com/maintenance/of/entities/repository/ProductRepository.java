package com.maintenance.of.entities.repository;
import com.maintenance.of.entities.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product,Long> {

    Product findByIdAndState(Long id, Boolean state);

    @Query(value = "SELECT COUNT(c.id) FROM Product c  WHERE c.name=:name")
    Integer findName(@Param("name") String name) throws Exception;
}
