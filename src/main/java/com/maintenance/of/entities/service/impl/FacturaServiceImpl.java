package com.maintenance.of.entities.service.impl;

import com.maintenance.of.entities.domain.Category;
import com.maintenance.of.entities.domain.Client;
import com.maintenance.of.entities.domain.Factura;
import com.maintenance.of.entities.dto.request.ClientDtoRequest;
import com.maintenance.of.entities.dto.request.ClientDtoRequestFactura;
import com.maintenance.of.entities.dto.request.FacturaDtoRequest;
import com.maintenance.of.entities.dto.request.ProductDtoRequest;
import com.maintenance.of.entities.dto.response.ClientDtoResponse;
import com.maintenance.of.entities.dto.response.FacturaDtoResponse;
import com.maintenance.of.entities.repository.ClientRepository;
import com.maintenance.of.entities.repository.FacturaRepository;
import com.maintenance.of.entities.service.FacturaService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class FacturaServiceImpl implements FacturaService {

    private final FacturaRepository facturaRepository;
    private final ClientRepository clientRepository;

    @Value("${zoneId}")
    private String zoneId;

    @Override
    public Object findFacturaByInvoiceNumber(String invoiceNumber) throws Exception {

        Factura factura = facturaRepository.findByInvoiceNumber(invoiceNumber);

        if (Objects.isNull(factura)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND);
        }
        Client client = clientRepository.findById(factura.getClient().getId()).get();
        ClientDtoRequestFactura clientDtoRequest=  ClientDtoRequestFactura.builder()
               .id(client.getId())
               .build();

        return FacturaDtoResponse.builder()
                .id(factura.getId())
                .invoiceNumber(factura.getInvoiceNumber())
                .client(clientDtoRequest)
                .createdAt(factura.getCreatedAt())
                .build();

    }

    @Override
    public Object saveFactura(FacturaDtoRequest facturasave) throws Exception {
        Optional<Factura> optionalFactura = Optional.ofNullable(facturaRepository.findByInvoiceNumber(facturasave.getInvoiceNumber()));

        if (optionalFactura.isPresent()){
            return ResponseEntity.status(HttpStatus.IM_USED);
        }

        Optional<Client> clientOptional = clientRepository.findById(facturasave.getClient().getId());

        if (clientOptional.isPresent()){

            Client client = clientOptional.get();
            ClientDtoRequestFactura clientDtoRequestFactura = ClientDtoRequestFactura.builder()
                    .id(client.getId())
                    .build();

          Factura factura = Factura.builder()
                    .invoiceNumber(facturasave.getInvoiceNumber())
                    .client(client)
                    .createdAt(Date.from(ZonedDateTime.now(ZoneId.of(zoneId)).toInstant()))
                    .build();
            return facturaRepository.save(factura);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND);

    }
}
