package com.maintenance.of.entities.repository;

import com.maintenance.of.entities.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findByIdAndState(Long id, Boolean state);

    @Query(value = "SELECT COUNT(c.id) FROM Category c  WHERE c.name=:name")
    Integer findName(@Param("name") String name) throws Exception;
}
