package com.maintenance.of.entities.dto.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ProductDtoRequest {

    private String name;
    private double price;
    private CategoryDtoRequestProduct category;

}