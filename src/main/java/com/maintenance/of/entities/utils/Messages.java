package com.maintenance.of.entities.utils;

public class Messages {
    public static final String ERROR_VALIDATION = "Validación fallida";
    public static final String ERROR_VIOLATION = "Violación de restricciones";

    public static final String PARAM_PAGE = "page: No debe ser nulo, blanco o vacío";
    public static final String PARAM_SIZE = "size: No debe ser nulo, blanco o vacío";
    public static final String PARAM_ID = "id No debe ser nulo, blanco o vacío";
    public static final String PARAM_STATE = "state: Debe ser un boleano";
    public static final String PARAM_FILTER = "No se permite nulo en el filtro";

    public static final String ERROR_SAVE = "No se pudo realizar el guardado";
    public static final String ERROR_UPDATE = "No se pudo realizar el actualizado";
    public static final String LIST_NULL = "La lista no debe de ser nulo";
    public static final String LIST_SIZE = "La lista debe ser mayor que cero";

    public static final String ERROR_ID = "El Id no existe";
    public static final String ID_REQUIRED = "Id: No debe ser nulo,blanco o vacìo";

    //CATEGORY
    public static final String CATEGORY_NAME_REQUIRED = "Name: No debe ser nulo,blanco o vacìo";
    public static final String CATEGORY_NAME_SIZE = "Name: Permite máximo 20 caracteres";

    //CLIENT
    public static final String CLIENT_NAME_REQUIRED = "Name: No debe ser nulo,blanco o vacìo";
    public static final String CLIENT_NAME_SIZE = "Name: Permite máximo 20 caracteres";
    public static final String CLIENT_ADDRESS_REQUIRED = "Address: No debe ser nulo,blanco o vacìo";
    public static final String CLIENT_ADDRESS_SIZE = "Address: Permite máximo 30 caracteres";
    public static final String CLIENT_PHONE_REQUIERED = "Phone: No debe ser nulo,blanco o vacìo, solo se admiten numeros";
    public static final String CLIENT_PHONE_SIZE = "Phone: Permite máximo 9 caracteres";


}
