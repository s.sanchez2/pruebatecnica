package com.maintenance.of.entities.utils;

public class Codes {
    // GENERAL
    public static final String SUCCESS_SAVE = "SUC_001";
    public static final String SUCCESS_UPDATE = "SUC_001";
    public static final String OBJECT_DUPLICATED = "OBJ_001";

    public static final String ERROR_SAVE = "ERR_001";
    public static final String ERROR_UPDATE = "ERR_002";

    // CATEGORY
    public static final String NOT_FOUND = "CAT_ERR_001";
    public static final String ERROR_ID = "C_ERR_001";


}