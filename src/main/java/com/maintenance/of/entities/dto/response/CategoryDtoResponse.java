package com.maintenance.of.entities.dto.response;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class CategoryDtoResponse {
    private Long id;
    private String name;
    private Boolean state;
    private Date createdAt;
    private Date modifiedAt;
}
