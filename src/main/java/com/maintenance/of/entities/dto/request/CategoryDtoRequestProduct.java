package com.maintenance.of.entities.dto.request;

import lombok.*;

import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class CategoryDtoRequestProduct {
    private Long id;
}
