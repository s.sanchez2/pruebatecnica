package com.maintenance.of.entities.service;

import com.maintenance.of.entities.dto.request.FacturaDtoRequest;
import com.maintenance.of.entities.dto.request.ProductDtoRequest;

public interface FacturaService {

    Object findFacturaByInvoiceNumber(String number) throws Exception;
    Object saveFactura(FacturaDtoRequest facturasave) throws Exception;

}
