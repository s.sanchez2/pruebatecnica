package com.maintenance.of.entities.dto.response;

import com.maintenance.of.entities.domain.Category;
import com.maintenance.of.entities.dto.request.CategoryDtoRequest;
import lombok.*;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ProductDtoResponse {

    private Long id;
    private String name;
    private double price;
    private CategoryDtoRequest category;
    private Boolean state;
    private Date createdAt;
    private Date modifiedAt;
}