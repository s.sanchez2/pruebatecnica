package com.maintenance.of.entities.dto.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ClientDtoRequest {

    private String name;
    private String address;
    private String phone;

}
